import { Ingreso } from "./menu.model";

export class IngresoEvento{
    ingresos: Ingreso[]= [
        new Ingreso("Acercamiento Sat a la nube", "Harold Cancinos","29/6/2022", "La capacitacion se llevara a cabo de forma presencial", "https://meet.google.com/qqu-rcsa-xsc")
    ];

    eliminar(ingreso:Ingreso){
    const indice: number = this.ingresos.indexOf(ingreso);
    this.ingresos.splice(indice,1);
    }

    
}
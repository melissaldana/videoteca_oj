import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ingreso } from './menu.model';
import { IngresoEvento } from './menu.servicio';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
 
  mostrar: Boolean= false;
  document: any;
  flag = false;
  ingresos:Ingreso[]=[];

  nombreInput: string ="";
  ponenteInput: string ="";
  fechaInput!: string;
  descripcionInput!: string;
  virtualInput!: string;
 
  agregarValor(){
    this.ingresoEvento.ingresos.push(new Ingreso(this.nombreInput, this.ponenteInput, this.fechaInput, this.descripcionInput, this.virtualInput));
  }



  cambiarFlag(){
    this.flag = !this.flag;
  }

  mostrarForm(){
    if (this.mostrar) {
      this.mostrar = false;
      this.document.getElementById('Formulario').style.display = "block";
      
      
    }else{
      this.mostrar = true;
      this.document.getElementById('Formulario').style.display = "block";
      
    }
  }

  cerrarForm(){
    if (this.mostrar) {
      this.mostrar = false; 
      this.document.getElementById('Formulario').style.display = "none"; 
    }else{
      this.mostrar = true;
      this.document.getElementById('Formulario').style.display = "none";
   
    }
  }


  constructor(private ingresoEvento:IngresoEvento) {
    this.ingresos = ingresoEvento.ingresos;
  }



  ngOnInit(){
    this.ingresos= this.ingresoEvento.ingresos;
  }

  eliminarRegistro(ingreso: Ingreso){
    this.ingresoEvento.eliminar(ingreso);
  }

}

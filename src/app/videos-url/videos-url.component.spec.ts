import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideosURLComponent } from './videos-url.component';

describe('VideosURLComponent', () => {
  let component: VideosURLComponent;
  let fixture: ComponentFixture<VideosURLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideosURLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideosURLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

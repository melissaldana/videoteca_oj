import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-videos-url',
  templateUrl: './videos-url.component.html',
  styleUrls: ['./videos-url.component.css']
})
export class VideosURLComponent implements OnInit {
  flag = false;
  mostrar: Boolean= false;
  document: any;


  mostrarForm(){
    if (this.mostrar) {
      this.mostrar = false;
      this.document.getElementById('Formulario').style.display = "block";
      
      
    }else{
      this.mostrar = true;
      this.document.getElementById('Formulario').style.display = "block";
      
    }
  }
  
  cerrarForm(){
    if (this.mostrar) {
      this.mostrar = false; 
      this.document.getElementById('Formulario').style.display = "none"; 
    }else{
      this.mostrar = true;
      this.document.getElementById('Formulario').style.display = "none";
   
    }
  }



  
  cambiarFlag(){
    this.flag = !this.flag;
  }
  constructor() { }

  ngOnInit(): void {
  }

}


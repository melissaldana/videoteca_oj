import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { VideosComponent } from './videos/videos.component';
import { VideosURLComponent } from './videos-url/videos-url.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent}, 
  { path: 'menu', component: MenuComponent },
  { path: 'videos', component: VideosComponent },
  { path: 'videos-url', component: VideosURLComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

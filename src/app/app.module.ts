import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { Ingreso } from './menu/menu.model';
import { IngresoEvento } from './menu/menu.servicio';
import { VideosComponent } from './videos/videos.component';
import { VideosURLComponent } from './videos-url/videos-url.component';
import { IngresoVideo } from './videos/videos.servicio';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    VideosComponent,
    VideosURLComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [IngresoEvento,IngresoVideo],
  bootstrap: [AppComponent]
})
export class AppModule { }

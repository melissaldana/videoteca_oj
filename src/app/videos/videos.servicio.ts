import { IngresoVideos } from "./videos.model";

export class IngresoVideo{
    ingresos: IngresoVideos[]= [
        new IngresoVideos("Acercamiento SAT a la nube", "Harold Cancinos", "Jurisdiccional", "50:59 minutos","Presencial")
    ];

    eliminar(ingreso:IngresoVideos){
    const indice: number = this.ingresos.indexOf(ingreso);
    this.ingresos.splice(indice,1);
    }

}
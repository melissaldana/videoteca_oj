import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { IngresoVideo } from './videos.servicio';
import { IngresoVideos } from './videos.model';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {
  flag = false;
  mostrar: Boolean= false;
  document: any;
  ingresos:IngresoVideos[]=[];
  nombreInput: string ="";
  ponenteInput: string ="";
  duracionInput!: string;
  descripcionInput!: string;
  categoriaInput!: string;
  
  cerrarForm(){
    if (this.mostrar) {
      this.mostrar = false; 
      this.document.getElementById('Formulario').style.display = "none"; 
    }else{
      this.mostrar = true;
      this.document.getElementById('Formulario').style.display = "none";
   
    }
  }
 
  agregarValor(){
    this.ingresoVideo.ingresos.push(new IngresoVideos(this.nombreInput, this.ponenteInput,this.categoriaInput, this.duracionInput, this.descripcionInput));
  }

  mostrarForm(){
    if (this.mostrar) {
      this.mostrar = false;
      this.document.getElementById('Formulario').style.display = "block";
      
      
    }else{
      this.mostrar = true;
      this.document.getElementById('Formulario').style.display = "block";
      
    }
  }
  
  
  cambiarFlag(){
    this.flag = !this.flag;
  }


  constructor(private ingresoVideo:IngresoVideo) {
    this.ingresos = ingresoVideo.ingresos;
  }

  ngOnInit(): void {
    this.ingresos= this.ingresoVideo.ingresos;
  }


  

}
